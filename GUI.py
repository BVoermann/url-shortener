import tkinter as tk
from tkinter import ttk, END
from shortener import shorten
from obtainer import obtain_url
from automate_naming import automate_naming

root = tk.Tk()
root.title('URL Shortener')
root.geometry('270x270')
tabControl = ttk.Notebook(root)

tab1 = ttk.Frame(tabControl)
tab2 = ttk.Frame(tabControl)

tabControl.add(tab1, text='Shortener')
tabControl.add(tab2, text='Obtainer')
tabControl.pack(expand=1, fill="both")

link_default = "Hier einen Link einfügen..."
name_default = "Hier den Namen einfügen..."
name_output_default = "Gib hier den Namen ein, dessen Link du möchtest..."

global rows
rows = []


def on_click_link(event):
    if link_field.get() == link_default:
        link_field.delete(0, END)
        link_field.config(foreground='black')


def on_click_name(event):
    if name_field.get() == name_default:
        name_field.delete(0, END)
        name_field.config(foreground='black')


def on_click_name_output(event):
    if name_field_output.get() == name_output_default:
        name_field_output.delete(0, END)
        name_field_output.config(foreground='black')


# generate tab1
link_field = tk.Entry(tab1, foreground='grey')
link_field.insert(0, link_default)
link_field.bind("<FocusIn>", on_click_link)
link_field.grid(column=0, row=0, padx=5, pady=30)

name_field = ttk.Entry(tab1, foreground='grey')
name_field.insert(0, name_default)
name_field.bind("<FocusIn>", on_click_name)
name_field.grid(column=0, row=2, padx=30, pady=30)

# generate tab2
name_field_output = ttk.Entry(tab2, foreground='grey')
name_field_output.insert(0, name_output_default)
name_field_output.bind("<FocusIn>", on_click_name_output)
name_field_output.grid(column=0, row=0, padx=30, pady=10)

link_field_output = tk.Text(tab2, foreground='green', width=30, height=4)
link_field_output.grid(column=0, row=1, padx=10, pady=10)


# generate buttons

B = tk.Button(tab1, text="Put the link/name pair in the DB!", command=lambda: shorten(link_field.get(), name_field.get()))
B.grid(column=0, row=3, padx=10, pady=10)
BO = tk.Button(tab2, text="Get the Link from a name!", command=lambda: obtain_url_update_var(name_field_output.get()))
BO.grid(column=0, row=3, padx=10, pady=10)
CB = tk.Button(tab2, text="Copy!", command=lambda: copy_func(link_field_output.get(1.0, END)))
CB.grid(column=0, row=2, padx=3, pady=3)
TB = tk.Button(tab1, text="Get title as name", command=lambda: automate(link_field.get()))
TB.grid(column=0, row=1)


def obtain_url_update_var(name):
    result = obtain_url(name)
    link_field_output.delete(1.0, END)
    link_field_output.insert(1.0, result)
    link_field_output.update()
    root.update_idletasks()


def automate(url):
    result = automate_naming(url)
    if result:
        on_click_name(url)
        name_field.insert(0, result)


def copy_func(link):
    root.clipboard_clear()
    root.clipboard_append(link)
    root.update()


root.mainloop()
