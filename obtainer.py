import sqlite3


def obtain_url(name):
    conn = sqlite3.connect('shortener.db')
    c = conn.cursor()
    entire_table = c.execute("SELECT * FROM url_shortener")
    name_for_url = name
    c.execute("SELECT link FROM url_shortener WHERE short = ?", (name_for_url,))
    rows = c.fetchall()
    if entire_table == 0:
        answer = "There is nothing in the database yet."
        rows = answer
        return rows
    elif not rows:
       answer = "Your name has no URL attached to it."
       rows = answer
       return rows
    else:
       return rows
