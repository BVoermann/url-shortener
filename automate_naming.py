from bs4 import BeautifulSoup
import requests
from tkinter import messagebox as mb


def automate_naming(url):
    try:
        code = requests.get(url).status_code
    except:
        code = 404

    if code == 200:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        title = soup.find('title').text
        return title
    else:
        mb.showinfo("Error", "This is not a valid URL")