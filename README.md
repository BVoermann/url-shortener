<b>About the project</b>

This project aims to show my familiarity with tkinter, sqlite3 and python
in general. 

To start the project, execute the GUI.py. There are no external 
libraries required for this project. The python version used is: 3.10.11.


<b>The scope of the project</b>

The scope of the project is to allow the user to store links in a database
and put a signifier in form of a name in too, so that, later, they can 
get the link back by typing in the name.


<b>What's left to do</b>

One could program a more precise automated naming than just taking 
the title of the URL.

Also, there could be the option to delete certain link/name pairs from the 
DB, although this has low priority. 


<b>Known bugs</b>

There are no known bugs.
