import sqlite3

def shorten(link, short):

    conn = sqlite3.connect('shortener.db')
    c = conn.cursor()

    c.execute('''
        CREATE TABLE IF NOT EXISTS url_shortener (
            id INTEGER PRIMARY KEY,
            link TEXT NOT NULL,
            short TEXT NOT NULL
        )
    ''')
    c.execute("DELETE FROM url_shortener WHERE (link, short) = (?,?)", (link, short))
    c.execute("INSERT INTO url_shortener (link, short) VALUES (?, ?)", (link, short))
    conn.commit()
    c.close()
    conn.close()
